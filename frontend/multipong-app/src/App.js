import React from 'react';
import './App.css';
import MainPage from './components/MainPage';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className='App'>
        <Switch>
          <Route path='/' exact component={MainPage} />

        </Switch>

      </div>
    </Router>
  );
}

export default App;
